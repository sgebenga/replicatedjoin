package replicatedjoin;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.StringUtils;

public class User {

    private final String name;
    private final int age;
    private final String state;

    public User(String name, int age, String state) {
        this.name = name;
        this.age = age;
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getState() {
        return state;
    }

    public static User fromString(String s) {
        String[] fields = StringUtils.split(s,'\t');
        return new User(fields[0], Integer.valueOf(fields[1]), fields[2]);
    }

    public static User fromText(Text text) {
        return fromString(text.toString());
    }

    @Override
    public String toString() {
        return String.format("%s\t%d\t%s", name, age, state);
    }
}
