package replicatedjoin;


import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.StringUtils;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class Application extends Configured implements Tool {


    public static class JoinMapper extends Mapper<LongWritable, Text, Text, Text> {
        public static final String DISTCACHE_FILENAME = "replicatedjoin.distcache.filename";
        private Map<String, User> users = new HashMap<String, User>();

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            URI[] files = context.getCacheFiles();

            final String distributedCacheFilename = context.getConfiguration().get(DISTCACHE_FILENAME);

            boolean found = false;

            FileSystem fs = FileSystem.get(context.getConfiguration());


            for (URI uri : files) {
                File path = new File(uri.getPath());

                if (path.getName().equals(distributedCacheFilename)) {
                    // loadCache(path);
                    FileStatus[] statuses = fs.listStatus(new Path(uri));
                    for (FileStatus status : statuses) {
                        loadCache2(fs, status, context);
                    }
                    found = true;
                    break;
                }
            }

            if (!found) {
                throw new IOException(String.format("Unable to find cache file %s", distributedCacheFilename));
            }
        }

        private void loadCache(File path) throws IOException {
            for (String line : FileUtils.readLines(path)) {
                User user = User.fromString(line);
                users.put(user.getName(), user);
            }
        }

        private void loadCache2(FileSystem fs, FileStatus status, Context context) throws IOException {
            try (BufferedReader reader = new BufferedReader((new InputStreamReader(fs.open(status.getPath()))))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    User user = User.fromString(line);
                    users.put(user.getName(), user);
                }
            }
        }

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            UserLog userLog = UserLog.fromText(value);
            User user = users.get(userLog.getName());
            if (user != null) {
                context.write(new Text(user.toString()), new Text(userLog.toString()));
            }
        }
    }

    public int run(final String[] strings) throws Exception {


        Path usersPath = new Path(strings[0]); //cache
        Path userLogsPath = new Path(strings[1]);
        Path outputPath = new Path(strings[2]);


        Configuration conf = getConf();
        Job job = Job.getInstance(conf);

        job.setJarByClass(Application.class);
        job.setMapperClass(JoinMapper.class);

        job.addCacheFile(usersPath.toUri());
        job.getConfiguration().set(JoinMapper.DISTCACHE_FILENAME, usersPath.getName());

        job.setNumReduceTasks(0);

        FileInputFormat.addInputPath(job, userLogsPath);
        FileOutputFormat.setOutputPath(job, outputPath);

        FileSystem fileSystem = FileSystem.get(conf);
        if (fileSystem.exists(outputPath)) {
            fileSystem.delete(outputPath, true);
        }


        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] strings) throws Exception {
        int exitCode = ToolRunner.run(new Application(), strings);
        System.exit(exitCode);
    }
}
