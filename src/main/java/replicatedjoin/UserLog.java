package replicatedjoin;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.StringUtils;

public class UserLog {

    private final String name;
    private final String event;
    private final String ipAddress;

    public UserLog(String name, String event, String ipAddress) {
        this.name = name;
        this.event = event;
        this.ipAddress = ipAddress;
    }

    public String getName() {
        return name;
    }

    public String getEvent() {
        return event;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public static UserLog fromString(String s) {
        String[] fields = StringUtils.split(s,'\t');
        return new UserLog(fields[0], fields[1], fields[2]);
    }

    public static UserLog fromText(Text t) {
        return fromString(t.toString());
    }

    @Override
    public String toString() {
        return String.format("%s\t%s\t%s", name, event, ipAddress);
    }
}
